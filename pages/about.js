import Link from "next/link";

class about extends React.Component {
  static async getInitialProps(props) {
    let req = props.req;
    let isServer = req != null;
    return { isServer };
  }

  componentDidMount() {}

  render() {
    const { isServer } = this.props;
    return (
      <div>
        <div>Hello World from Redso</div>
        <div>Page: About</div>
        {isServer && <div>Render from server</div>}
        <Link href="/dev/">
          <a>Home Page</a>
        </Link>
      </div>
    );
  }
}

export default about;
