import Document, { Html, Head, Main, NextScript } from "next/document";

class _document extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head>
          <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
          <script src="https://accounts-res-uat.hktdc.com/assets/javascripts/client/sso.js"></script>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default _document;
