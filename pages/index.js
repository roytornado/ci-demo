import Link from "next/link";

class index extends React.Component {
  static async getInitialProps(props) {
    let req = props.req;
    let isServer = req != null;
    return { isServer };
  }

  constructor(props) {
    super(props);
    this.state = { msg: "N/A", message: "LoginFromFrontend" };
  }

  componentDidMount() {}

  render() {
    const { isServer } = this.props;
    return (
      <div>
        <div>GitLab CI/CD Demo from Redso #2</div>
        <div>Page: Home</div>
        {isServer && <div>Render from server</div>}
        <Link href="/dev/about">
          <a>About Page</a>
        </Link>
        <br />
        <button onClick={this.callAPI.bind(this)}>Demo API Call</button>
        <br />
        <div>Response: {this.state.msg}</div>
        <br />
        <br />
        <p>=========================================</p>
        <p>ADFS Demo</p>
        <p>
          Message:{" "}
          <input
            type="text"
            name="message"
            value={this.state.message}
            onChange={this.handleChange.bind(this)}
          />
        </p>
        <button onClick={this.loginADFS.bind(this)}>Login (ADFS)</button>
        <p>=========================================</p>
        <p>=========================================</p>
        <p>SSO Demo</p>
        <button onClick={this.checkSSO.bind(this)}>Check</button>
        <p>=========================================</p>
      </div>
    );
  }

  callAPI() {
    //window.alert("callAPI");
    //const axios = import("axios").default;
    let setState = this.setState.bind(this);
    setState({ msg: "Loading" });
    fetch("https://flask-demo.redsoapp.com/")
      .then(function(response) {
        response.text().then(value => {
          console.log(value);
          setState({ msg: value });
        });
      })
      .catch(function(error) {
        console.log(error);
        setState({ msg: "Error" });
      });
  }

  handleChange(event) {
    this.setState({ message: event.target.value });
  }

  loginADFS() {
    console.log(this.state.message);
    window.location.replace(
      "https://tdc-spcs.redsoapp.com/adfs_login?msg=" + this.state.message
    );
  }

  checkSSO() {
    console.log("checkSSO");
    var token = sso.client.getAccessToken();
    console.log(token);
    window.alert(token);
    //sso.client.login({ lang: "zh-tw", redirect_uri: "http://localhost:3010/" });
    if (token) {
      fetch("https://api-sso-uat.hktdc.com/v1/bearer/user-info", {
        headers: {
          Authorization: "Bearer " + token
          // 'Content-Type': 'application/x-www-form-urlencoded',
        }
      }).then(response => console.log(response));
    }
  }
}

export default index;
